
require('dotenv').config();

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var helmet = require('helmet');
var cors = require('cors');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var expeditionsRouter = require('./routes/expeditions');
var userRouter = require('./routes/users');
var stepRouter = require('./routes/steps');
var traceRouter = require('./routes/traces');
var waypointRouter = require('./routes/waypoints');

var app = express();

// Set up mongoose connection
var mongoose = require('mongoose');
var mongoDB = 'mongodb://localhost/nomadmaps';
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// secure apps by setting various HTTP headers
app.use(helmet());

// enable CORS - Cross Origin Resource Sharing
app.use(cors());

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/expeditions', expeditionsRouter);
app.use('/users', userRouter);
app.use('/steps', stepRouter);
app.use('/traces', traceRouter);
app.use('/waypoints', waypointRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // send error
  res.status(err.status || 500);
  res.json({ message: err.message || 'error' });
});

module.exports = app;
