
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var Expedition = require('../models/expedition')

exports.getAll = async function(req, res, next) {
  var ids = req.query.ids ? req.query.ids.split(',') : [];

  var validIds = ids
    .filter(e => /^[a-fA-F0-9]{24}$/.test(e))
    .map(e => new ObjectId(e));

  var query = Expedition.find({});
  query = ids.length ? query.where('_id').in(validIds) : query;

  var expeditions = await query.exec();
  res.json(expeditions);
};

exports.getById = async function(req, res, next) {
  if (!/^[a-fA-F0-9]{24}$/.test(req.params.id)) {
    return next(createError(404, `No expedition found with id ${req.params.id}`));
  }

  var expedition = await Expedition.findById(new ObjectId(req.params.id))
  res.json(expedition)
};
