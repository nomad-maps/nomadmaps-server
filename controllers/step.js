
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var Step = require('../models/step');
var docsToGeoJson = require('../util/docsToGeoJson');

exports.search = async function(req, res, next) {
  var expeditions = req.query.expeditions ? req.query.expeditions.split(',') : [];

  var validExpeditions = expeditions
    .filter(e => /^[a-fA-F0-9]{24}$/.test(e))
    .map(e => new ObjectId(e));

  var query = Step.find({});
  query = expeditions.length ? query.where('expedition').in(validExpeditions) : query;

  var steps = await query.exec();
  res.json(docsToGeoJson(steps));
};

exports.getById = async function(req, res, next) {
  if (!/^[a-fA-F0-9]{24}$/.test(req.params.id)) {
    return next(createError(404, `No step found with id ${req.params.id}`));
  }

  var step = await Step.findById(new ObjectId(req.params.id));
  res.json(docsToGeoJson([step]).features[0]);
};
