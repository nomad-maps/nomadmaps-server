
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var Trace = require('../models/trace');
var docsToGeoJson = require('../util/docsToGeoJson');

exports.search = async function(req, res, next) {
  var expeditions = req.query.expeditions ? req.query.expeditions.split(',') : [];

  var validExpeditions = expeditions
    .filter(e => /^[a-fA-F0-9]{24}$/.test(e))
    .map(e => new ObjectId(e));

  var query = Trace.find({});
  query = expeditions.length ? query.where('expedition').in(validExpeditions) : query;

  var traces = await query.exec();
  res.json(docsToGeoJson(traces));
};

exports.getById = async function(req, res, next) {
  if (!/^[a-fA-F0-9]{24}$/.test(req.params.id)) {
    return next(createError(404, `No trace found with id ${req.params.id}`));
  }

  var trace = await Trace.findById(new ObjectId(req.params.id));
  res.json(docsToGeoJson([trace]).features[0]);
};
