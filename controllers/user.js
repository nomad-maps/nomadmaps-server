
var createError = require('http-errors');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var User = require('../models/user');

exports.search = async function(req, res, next) {
  var ids = req.query.ids ? req.query.ids.split(',') : [];

  var validIds = ids
    .filter(e => /^[a-fA-F0-9]{24}$/.test(e))
    .map(e => new ObjectId(e));

  var query = User.find({});
  query = ids.length ? query.where('_id').in(validIds) : query;

  var users = await query.exec();
  res.json(users);
};

exports.getById = async function(req, res, next) {
  if (!/^[a-fA-F0-9]{24}$/.test(req.params.id)) {
    return next(createError(404, `No user found with id ${req.params.id}`));
  }

  var user = await User.findById(new ObjectId(req.params.id));
  res.json(user);
};
