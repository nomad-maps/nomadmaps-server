
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var Waypoint = require('../models/waypoint');
var docsToGeoJson = require('../util/docsToGeoJson');

exports.search = async function(req, res, next) {
  var traces = req.query.traces ? req.query.traces.split(',') : [];
  var validTraces = traces
    .filter(e => /^[a-fA-F0-9]{24}$/.test(e))
    .map(e => new ObjectId(e));

  var query = Waypoint.find({})
  query = traces.length ? query.where('trace').in(validTraces) : query;

  var waypoints = await query.exec();
  res.json(docsToGeoJson(waypoints));
};

exports.getById = async function(req, res, next) {
  if (!/^[a-fA-F0-9]{24}$/.test(req.params.id)) {
    return next(createError(404, `No waypoint found with id ${req.params.id}`));
  }

  var waypoint = await Waypoint.findById(new ObjectId(req.params.id));
  res.json(docsToGeoJson([waypoint]).features[0]);
};
