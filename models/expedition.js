
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ExpeditionSchema = new Schema({
  name: {
    type: String,
    required: true,
    max: 150
  },

  user: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },

  start_date: {
    type: Date,
    required: true
  },

  end_date: {
    type: Date,
    required: true
  },

  active: {
    type: Boolean,
    default: false
  }
})

ExpeditionSchema.set('toJSON', {
  virtuals: true,
  versionKey:false,
  transform: function (doc, ret) {  delete ret._id  }
});

module.exports = mongoose.model('Expedition', ExpeditionSchema);
