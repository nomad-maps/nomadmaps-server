
var mongoose = require('mongoose');
var PointSchema = require('../schemas/Point')

var Schema = mongoose.Schema;

var StepSchema = new Schema({
  name: {
    type: String,
    required: true,
    max: 150
  },

  expedition: {
    type: Schema.ObjectId,
    ref: 'Expedition',
    required: true
  },

  arrival_date: {
    type: Date,
    required: true
  },

  number: {
    type: Number,
    required: true
  },

  geometry: {
    type: PointSchema,
    required: true
  }
})

StepSchema.set('toJSON', {
  virtuals: true,
  versionKey:false,
  transform: function (doc, ret) { delete ret._id }
});

module.exports = mongoose.model('Step', StepSchema);
