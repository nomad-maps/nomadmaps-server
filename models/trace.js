
var mongoose = require('mongoose');
var LineStringSchema = require('../schemas/LineString')

var Schema = mongoose.Schema;

var TraceSchema = new Schema({
  trace_id: {
    type: String,
    required: true,
    max: 12
  },

  expedition: {
    type: Schema.ObjectId,
    ref: 'Expedition',
    required: true
  },

  date: {
    type: Date,
    required: true
  },

  tags: {
    type: [String],
    required: true
  },

  geometry: {
    type: LineStringSchema,
    required: true
  }
})

TraceSchema.set('toJSON', {
  virtuals: true,
  versionKey:false,
  transform: function (doc, ret) {  delete ret._id  }
});

module.exports = mongoose.model('Trace', TraceSchema);
