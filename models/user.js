
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var UserSchema = new Schema({
  name: {
    type: String,
    required: true,
    max: 100
  },

  osm_uid: {
    type: String,
    required: true,
    max: 12
  },

  osm_user: {
    type: String,
    required: true,
    max: 50
  },

  mapillary_user: {
    type: String,
    required: true,
    max: 50
  },

  flickr_uid: {
    type: String,
    required: true,
    max: 20
  }
})

UserSchema.set('toJSON', {
  virtuals: true,
  versionKey:false,
  transform: function (doc, ret) {  delete ret._id  }
});

module.exports = mongoose.model('User', UserSchema);
