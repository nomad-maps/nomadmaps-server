
var mongoose = require('mongoose');
var PointSchema = require('../schemas/Point')

var Schema = mongoose.Schema;

var WayPointSchema = new Schema({
  z: {
    type: Number,
    required: true
  },

  time: {
    type: Date,
    required: true
  },

  trace: {
    type: Schema.ObjectId,
    ref: 'Trace',
    required: true
  },

  geometry: {
    type: PointSchema,
    required: true
  }
})

WayPointSchema.set('toJSON', {
  virtuals: true,
  versionKey:false,
  transform: function (doc, ret) {  delete ret._id  }
});

module.exports = mongoose.model('Waypoint', WayPointSchema);
