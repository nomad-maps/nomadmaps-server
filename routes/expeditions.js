var express = require('express');
var router = express.Router();

var expeditionController = require('../controllers/expedition');

/* GET expedition listing */
router.get('/', expeditionController.getAll);

// GET request for one expedition.
router.get('/:id', expeditionController.getById);

module.exports = router;
