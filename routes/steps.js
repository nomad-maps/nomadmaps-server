var express = require('express');
var router = express.Router();

var stepController = require('../controllers/step');

/* GET step listing */
router.get('/', stepController.search);

// GET request for one step.
router.get('/:id', stepController.getById);

module.exports = router;
