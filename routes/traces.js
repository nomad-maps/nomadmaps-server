var express = require('express');
var router = express.Router();

var traceController = require('../controllers/trace');

/* GET trace listing */
router.get('/', traceController.search);

// GET request for one trace.
router.get('/:id', traceController.getById);

module.exports = router;
