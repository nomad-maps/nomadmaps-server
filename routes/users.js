var express = require('express');
var router = express.Router();

var userController = require('../controllers/user');

/* GET user listing */
router.get('/', userController.search);

// GET request for one user.
router.get('/:id', userController.getById);

module.exports = router;
