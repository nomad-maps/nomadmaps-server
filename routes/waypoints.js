var express = require('express');
var router = express.Router();

var waypointController = require('../controllers/waypoint');

/* GET waypoint listing */
router.get('/', waypointController.search);

// GET request for one waypoint.
router.get('/:id', waypointController.getById);

module.exports = router;
