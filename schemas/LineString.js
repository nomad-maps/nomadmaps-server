
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var LineStringSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: ['LineString'],
    required: true
  },

  coordinates: {
    type: [[Number]],
    required: true
  }
});

module.exports = LineStringSchema;
