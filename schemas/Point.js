
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var PointSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: ['Point'],
    required: true
  },

  coordinates: {
    type: [Number],
    required: true
  }
});

PointSchema.set('toJSON', {
  virtuals: false,
  transform: function (doc, ret) {  delete ret._id  }
});

module.exports = PointSchema;
