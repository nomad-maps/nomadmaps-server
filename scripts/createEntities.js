
var mongoose = require('mongoose');
var fs = require('fs-extra');

var User = require('../models/user');
var Expedition = require('../models/expedition')
var Step = require('../models/step')

var mongoDB = 'mongodb://localhost/nomadmaps';
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

function format (geoJson) {
  return geoJson.features.map(feature => {
    const entity = Object.assign({}, feature.properties);
    entity.geometry = Object.assign({}, feature.geometry);
    return entity;
  })
}

async function createEntities (path, schema, isGeoJson) {
  const add = await fs.pathExists(path);

  if (add) {
    var json = await fs.readJson(path);
    var entities = isGeoJson ? format(json) : json
    await schema.insertMany(entities);
    await fs.remove(path);
  }
}

async function main () {
  await createEntities('./temp/users.json', User, false);
  await createEntities('./temp/expeditions.json', Expedition, false);
  await createEntities('./temp/steps.json', Step, true);
}

main ();
