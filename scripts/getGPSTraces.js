
const rp = require('request-promise')
const cheerio = require('cheerio')

async function getGPSTraces(osmUser) {
  const baseUrl = `https://www.openstreetmap.org/user/${osmUser}/traces/page/`
  let traces = []
  let nextPage = true
  let pageNb = 0

  while (nextPage) {
    pageNb++

    const pageUrl = `${baseUrl}${pageNb}`
    const $ = await rp({ uri: pageUrl, transform: body => cheerio.load(body)  })
    const paginationLinks = $('p').find('a').toArray()
    const trs = $('table#trace_list').find('tr').toArray()
    const validTrs = trs.slice(1)

    const tracesFromPage = validTrs.map((elm) => {
      const spans = $(elm).find('span').toArray()
      const links = $(elm).find('a').toArray()

      return {
        id: $(links[0]).attr('href').replace('/user/Nomadmapper/traces/', ''),
        date: $(spans[0]).attr('title').replace(' UTC', ''),
        tags: links.slice(5, links.length).map(l => $(links[5]).text())
      }
    })

    traces = traces.concat(tracesFromPage)

    nextPage = paginationLinks
      .filter(link => $(link).text() === 'Older Traces')
      .length > 0
  }

  return traces
}

module.exports = getGPSTraces
