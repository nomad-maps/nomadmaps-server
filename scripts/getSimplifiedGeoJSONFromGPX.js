
const simplify = require('simplify-js')
const moment = require('moment')

function toGeoJSON(point) {
  return {
    type: 'Feature',
    properties: { z: point.z, time: point.time },
    geometry: { type: 'Point', coordinates: [point.x, point.y] }
  }
}

function getSimplifiedGeoJSONFromGPX(gpx) {
  const segments = gpx.tracks[0].segments
  const waypoints = segments.reduce((acc, val) => acc.concat(val), [])
  const points = waypoints.map(wp => ({ x: wp.lon, y: wp.lat, z: wp.elevation, time: wp.time }))
  const simpPoints = simplify(points, 0.00008)
  const features = simpPoints.map(toGeoJSON)
  return { type: 'FeatureCollection', features: features }
}

module.exports = getSimplifiedGeoJSONFromGPX
