
var mongoose = require('mongoose');
var moment = require('moment')

var getGPSTraces = require('./getGPSTraces');
var parseRemoteGPXFile = require('./parseRemoteGPXFile');
var getSimplifiedGeoJSONFromGPX = require('./getSimplifiedGeoJSONFromGPX');

var ObjectId = mongoose.Types.ObjectId;
var User = require('../models/user');
var Expedition = require('../models/expedition');
var Trace = require('../models/trace');
var Waypoint = require('../models/waypoint');

var mongoDB = 'mongodb://localhost/nomadmaps';
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

function format (geoJson) {
  return geoJson.features.map(feature => {
    const entity = Object.assign({}, feature.properties);
    entity.geometry = Object.assign({}, feature.geometry);
    return entity;
  })
}

async function handleTrace(trace, expedition) {
  var url = `https://www.openstreetmap.org/trace/${trace.id}/data`;
  var gpx = await parseRemoteGPXFile(url);
  var geojson = getSimplifiedGeoJSONFromGPX(gpx);
  var waypoints = format(geojson);

  var coordinates = geojson.features
    .reduce((acc, val) => {
      acc.push(val.geometry.coordinates)
      return acc
    }, []);

  var traceToSave = new Trace({
    trace_id: trace.id,
    expedition: expedition._id,
    date: trace.date,
    tags: trace.tags,
    geometry: {
      type: 'LineString',
      coordinates: coordinates
    }
  });

  var savedTrace = await traceToSave.save();

  waypoints
    .forEach(w => { w.trace = savedTrace._id; });

  await Waypoint.insertMany(waypoints);
}

async function getRecordedTraces(osmUser, startDate, endDate) {
  var startTimestamp = moment(startDate).valueOf();
  var endTimestamp = moment(endDate).valueOf();
  var recordedTraces = await getGPSTraces(osmUser);

  return recordedTraces
    .filter(t => {
      var timestamp = moment(t.date).valueOf();
      return timestamp > startTimestamp && timestamp < endTimestamp;
    })
}

async function getTraces(expedition) {
  var user = await User
    .findById(new ObjectId(expedition.user));

  var recordedTraces = await getRecordedTraces(
    user.osm_user,
    expedition.start_date,
    expedition.end_date
  );

  var savedTraces = await Trace
    .find({ expedition: new ObjectId(expedition._id) });

  var savedTracesIds = savedTraces
    .map(t => t.trace_id);

  var traces = recordedTraces
    .filter(t => savedTracesIds.indexOf(t.id) < 0);

  for (i = 0; i < traces.length; i++) {
    await handleTrace(traces[i], expedition);
  }
}

async function main () {
  try {
    var activeExpeditions = await Expedition
      .find({ active: true });

    for (i = 0; i < activeExpeditions.length; i++) {
      await getTraces(activeExpeditions[i]);
    }
  } catch (e) {
    console.error(e);
  }
}

main();
