
const gpxParse = require('gpx-parse')

function parseRemoteGpx(url) {
  return new Promise(function(resolve, reject) {
    gpxParse.parseRemoteGpxFile(url, function(error, data) {
      if(error !== null) {
        reject(Error(error));
      } else {
        resolve(data);
      }
    })
  })
}

module.exports = parseRemoteGpx
