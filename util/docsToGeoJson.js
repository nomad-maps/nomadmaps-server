
module.exports = function (docs) {
  var features = docs
    .map(doc => doc.toJSON())
    .map(doc => ({
      type: 'Feature',
      geometry: doc.geometry,
      properties: Object.assign({}, doc, { geometry: undefined })
    }))

  return {
    type: 'FeatureCollection',
    features: features
  }
}
